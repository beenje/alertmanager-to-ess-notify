FROM python:3.9-slim

COPY requirements.txt /requirements.txt
RUN python3 -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir --upgrade pip \
  && pip install --no-cache-dir -r /requirements.txt

RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -r -g csi -u 1000 csi

COPY --chown=csi:csi . /app/
WORKDIR /app

ENV PATH /venv/bin:$PATH

USER 1000

# Running uvicorn is for testing
# For production, run using Gunicorn using the uvicorn worker class
# Use one or two workers per-CPU core
# For example:
# gunicorn -b 0.0.0.0:8000 -w 4 -k uvicorn.workers.UvicornWorker --log-level info app.main:app
# CMD ["uvicorn", "--proxy-headers", "--host", "0.0.0.0", "--port", "8000", "--log-level", "debug", "app.main:app"]
CMD ["gunicorn", "-b", "0.0.0.0:8000", "-w", "4", "-k", "uvicorn.workers.UvicornWorker", "--log-level", "debug", "app.main:app"]
