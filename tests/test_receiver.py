import os
from fastapi.testclient import TestClient

os.environ["APP_CONFIG_FILE"] = "tests/settings.yaml"


def test_receive(requests_mock, client: TestClient, fake_alert_group1):
    adapter = requests_mock.post(
        "https://notify.esss.lu.se/api/v2/services/fake_service_id/notifications"
    )
    # The following register_uri is needed to pass the request to the test TestClient
    requests_mock.register_uri("POST", "http://testserver/webhook", real_http=True)
    response = client.post("/webhook", json=fake_alert_group1)
    assert response.status_code == 200
    assert response.text == "true"
    assert adapter.call_count == 1


def test_receive_bad_request(requests_mock, client: TestClient):
    adapter = requests_mock.post(
        "https://notify.esss.lu.se/api/v2/services/fake_service_id/notifications"
    )
    # The following register_uri is needed to pass the request to the test TestClient
    requests_mock.register_uri("POST", "http://testserver/webhook", real_http=True)
    response = client.post("/webhook", json={})
    assert response.status_code == 422
    assert response.text != "true"
    assert adapter.call_count == 0
