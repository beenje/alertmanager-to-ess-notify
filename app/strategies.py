from enum import Enum
from typing import List, Dict
from .alertmanagerschemas import AlertGroup
from .essnotify import Notification


def _extract_csentry_groups(labels: Dict[str, str]) -> List[str]:
    prefix = "csentry_group_"
    csentry_group_list = [
        x[
            x.startswith(prefix)
            and len(prefix) :  # noqa: E203  black and flake8 are fighting
        ]
        for x in labels.keys()
        if x.startswith(prefix)
    ]
    return csentry_group_list


def strategy_one_to_one(alert_group: AlertGroup) -> List[Notification]:
    ret: List[Notification] = []

    for alert in alert_group.alerts:
        title = f'{alert.labels["alertname"]}'
        if "instance" in alert.labels.keys():
            title = f'{alert.labels["instance"]} - {title}'
        if "severity" in alert.labels.keys():
            title = f'{title} - {alert.labels["severity"]}'

        message = ""
        if "summary" in alert.annotations.keys():
            message = f"{message}{alert.annotations['summary']}\n"
        if (
            "descripton" in alert.annotations.keys()
            and alert.annotations["summary"] != alert.annotations["descripton"]
        ):
            message = f"{message}{alert.annotations['descripton']}\n"
        if len(_extract_csentry_groups(alert.labels)) > 0:
            message = f"{message}Ansible groups:\n"
            for group in _extract_csentry_groups(alert.labels):
                message = f"{message}{group}\n"

        ret.append(Notification(title=title, message=message))
    return ret


def strategy_emx(alert_group: AlertGroup) -> List[Notification]:
    ret: List[Notification] = []
    return ret


# FIXME: create enum from dict or something
class StartegyEnum(Enum):
    one_to_one = "one_to_one"
    emx = "emx"


STRATEGIES = {"one_to_one": strategy_one_to_one, "emx": strategy_emx}
