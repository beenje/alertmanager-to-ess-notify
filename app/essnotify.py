import json
import requests
from pydantic import BaseModel
from fastapi.logger import logger
from typing import Optional, Dict


class Notification(BaseModel):
    title: str
    message: str
    url: Optional[str] = None


def send_notification(notification: Notification, service_id: str, notify_baseurl: str):
    headers = {"accept": "application/json", "Content-Type": "application/json"}
    notify_url = notify_baseurl + "/" + service_id + "/notifications"
    payload = {"title": notification.title, "subtitle": notification.message}
    if notification.url is not None:
        payload["url"] = notification.url
    if _send_notification_real(
        url=notify_url, data=json.dumps(payload), headers=headers
    ):
        logger.info("Notification sent: %s", payload)
    else:
        logger.error("Notification NOT sent: %s", payload)


def _send_notification_real(url: str, data: str, headers: Dict) -> bool:
    try:
        req = requests.post(url, data=data, headers=headers)
        req.raise_for_status()
    except requests.exceptions.RequestException as exc:
        logger.exception(exc)
    except ConnectionError as exc:
        logger.exception(exc)
    else:
        return True
    return False
